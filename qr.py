#!/usr/bin/env python3
from bottle import route, run, template, abort, install, post, get
from bottle import static_file
from bottle import request, response, redirect

from bottle_sqlite import SQLitePlugin

import time
from datetime import datetime

import urllib.parse
import subprocess

### Constants
APP_ROOT = './'
DATABASE = APP_ROOT + 'qr.db'
DEBUG = True
QR_MAX_SIZE = 1024 * 3

### Database
#install(SQLitePlugin(dbfile=DATABASE))

### Debug functions ###
#######################
@route('/teapot')
def im_a_teapot():
    'Test function for dumping browser headers'
    if not DEBUG:
        return None
    print("--- Http headers: ---")
    for key in request.headers:
        print("  %s: %s" % (str(key), str(request.headers[key])))
    print("---------------------")
    abort(418, "I'm a teapot!")

### Helper functions ###
########################

def getBytes(text):
    return bytes(text, 'latin-1')

def dont_cache():
    "Add a don't cache header to response"
    response.headers["Cache-Control"] = "no-store"

### Site functions ###
######################
@route('/static/<filepath:path>')
def static_content(filepath):
    return static_file(filepath, root = APP_ROOT + 'static/')

@route('/qr')
def qr_picture():
  payload = urllib.parse.unquote_to_bytes(request.query.payload)
  if len(payload) == 0:
    payload = bytes("This QR feels empty.", "utf-8")
  if len(payload) > QR_MAX_SIZE:
    payload = payload[:QR_MAX_SIZE]
  rv = subprocess.run(['qrencode', '-t', 'png', '-s', '9', '-o', '-'], input=payload, capture_output=True)
  response.set_header('Content-Type', 'image/png')
  return rv.stdout

@post('/')
def qr_viewer():
  payload = getBytes(request.forms.get('payload'))
  if len(payload) > QR_MAX_SIZE:
    payload = payload[:QR_MAX_SIZE]
  payload_str = urllib.parse.quote(payload, safe="")
  return template('qr', payload=payload_str)

@get('/')
def root():
    return template('main')

@route('/about')
def about():
    return template('about')

if __name__ == '__main__':
    run(host='0.0.0.0', port=8800)
    #run(host='127.0.0.1', port=8800, server='paste')
