% include('header.tpl', title='About...')
<div class="container">
<h1>About...</h1>
<p>On this page, all the relevant third party licenses and acknowledgements will be listed.<p>
<h3 id="milligram-css">Milligram CSS</h3>
<p><a href="https://milligram.io">Milligram CSS</a> is Copyright (c) CJ Patoilo <a href="mailto:cjpatoilo@gmail.com" class="email">cjpatoilo@gmail.com</a> and licensed under the MIT license.</p>
<h3 id="qrencode">libqrencode</h3>
<p><a href="https://fukuchi.org/works/qrencode/">Libqrencode</a> is Copyright (c) Kentaro Fukuchi <a href="mailto:kentaro@fukuchi.org">kentaro@fukuchi.org</a> and licensed under the LGPL license.</p>
</div>
% include('footer.tpl')
