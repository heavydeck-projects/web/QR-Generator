%import os
%dbg = bool(os.getenv("WEB_DEBUG", default = None))
<!DOCTYPE html>
<!-- Begin header -->
<html lang={{get('lang', 'en')}}>
 <head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
  <link rel="stylesheet" href="/static/css/normalize.css">
  <link rel="stylesheet" href="/static/css/milligram.css">
  <link rel="stylesheet" href="/static/css/override.css">
  <title>{{get('title', 'No Title')}}</title>

 </head>
 <body>
  <main class="wrapper">
% include('nav.tpl')
<!-- End header -->
