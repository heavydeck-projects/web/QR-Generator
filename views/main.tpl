% include('header.tpl', title='QRcode generator')
<div class="container">
<form action="/" method="post">
  <fieldset>
    <label for="fieldData">Data</label>
    <input type="text" placeholder="Hello!" autocomplete="off" name="payload">
    <input class="button-primary" type="submit" value="Generate">
  </fieldset>
</form>
</div>
% include('footer.tpl')
